import styled from "styled-components";

export const MainStyles = styled.div`
  width: 70%;
  font-size: 4rem;
  margin: 5rem;
  text-align: center;
  @media screen and (max-width: 800px) {
    width: 100%;
    font-size: 1.5rem;
  }
`;
