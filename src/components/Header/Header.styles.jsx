import styled from "styled-components";

export const HeaderStyles = styled.div`
  font-size: 5rem;
  font-weight: bolder;
  margin: 5rem;
  @media screen and (max-width: 800px) {
    font-size: 1rem;
  }
`;
