import withBorder from "../../hooks/withBorder";
import TextMain from "../TextMain/TextMain";

const TextQuestion = (props) => withBorder(TextMain)(props);

export default TextQuestion;
