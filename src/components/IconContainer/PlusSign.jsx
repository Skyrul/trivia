import React from "react";

const PlusSign = () => {
  return (
    <img
      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAABDCAYAAADHyrhzAAAABmJLR0QA/wD/AP+gvaeTAAABP0lEQVR4nO3cIU4DQRhH8f8sELB13AFB0koEBoHkCLgKLIcgQWBbJIJzcIEiegaCRUJI6CA2KQ/VNZ1UvJ+e5Pvykk02mc0mWivNJ84nV0m52HywvmW6uNv+Qn/2Ww5LktScpdSbASeXSZrG6FoO23XGAGOAMcAYYAwwBhgDjAHGAGOAMcAYYAwwBhgDjAHGAGOAMcAYYAwwBhgDjAH9jdpsfJkux00m1noy8FZzlMfx9bbXWXt/feq3mo9fknLebPAuGn0d+piAMcAYYAwwBhgDjAHGgP4NtJaflKwazSwZ/mFdq52Sj6Pa/mu/2eQ+JbcDTi4zXZxufR/wMQFjgDHAGGAMMAYYA4wBxgBjgDHAGGAMMAYYA4wBxgBjgDHAGGAMMAYYA4wBxgBjQPv/ZxysHvK997zxXFc/G2zzzy+dDSJsai14CAAAAABJRU5ErkJggg=="
      alt="right"
    ></img>
  );
};

export default PlusSign;
