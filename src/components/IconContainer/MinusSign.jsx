import React from "react";

const MinusSign = () => {
  return (
    <img
      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAAVklEQVRoge3TQQqAMAwEwMUP+Bb//6p+oHrQW8mlIFaYgdx3Q5IAAAAAMO9I0pOci01/sg22osiepE0s4G0td7ZBVeR3FFlNVaS8xY+t+rsAAAAAP3EBNHEqRU8MfGsAAAAASUVORK5CYII="
      alt="wrong"
    ></img>
  );
};

export default MinusSign;
